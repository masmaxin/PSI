# 关于PSI

---

PSI是企业管理软件，是由艾格林门信息服务（大连）有限公司发起的商业项目。PSI基于开源技术，提供人、财、物、产、供、销、存一体化的企业管理全面解决方案。

# PSI的开源协议

PSI的开源协议为GPL v3

> 使用手册文档版本号：202107121605
